# Change log
All notable changes to this project will be documented in this file

## [0.1.0] - 2016-06-29
### Added
- Initial project deployment

## [1.0.0] - 2016-06-30
### Modified
- Changed behavior of static.json AJAX request to allow for safe object checking

## [1.0.1] - 2016-06-30
### Modified
- Changed behavior of static.json AJAX request to allow for safe object checking
