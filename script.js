(function(){
  // Global const
  var globals = {
    guid: 'm5be90792-7562-4435-ae9c-48deea1b3fc9', // guid for uniquely identifying the injected DOM element so that styles don't inadvertently apply elsewhere

    strMainHeadline: 'Amazon on Twitter',
    strTweetCap: '<a href="https://www.twitter.com/amazon" title="Follow Amazon on Twitter" target="_blank">Follow @amazon on Twitter and join the conversation</a>',

    constUrlLogoTwitter: chrome.extension.getURL('assets/twitter-bird@2x.png'),
    constUrlIconArrow: chrome.extension.getURL('assets/arrow@2x.png'),
    constUrlIconAmazon: chrome.extension.getURL('assets/amazon@2x.png'),
    constUrlDataStaticJson: chrome.extension.getURL('data/static.json'),

    constUriTwitterApi: 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20FROM%20twitter.search.tweets%20WHERE%20q%3D%22%23Amazon%22%20AND%20lang%3D%22en%22%20AND%20consumer_key%3D%2208ZNcNfdoCgYTzR7qcW1HQ%22%20AND%20consumer_secret%3D%22PTMIdmhxAavwarH3r4aTnVF7iYbX6BRfykNBHIaB8%22%20AND%20access_token%3D%221181240586-JIgvJe4ev3NHdHnAqnovHINWfpo0qB2S2kZtVRI%22%20AND%20access_token_secret%3D%221nodv0LBsi7jS93e38KiW8cHOA5iUc6FT4L6De7kgk%22&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys'
  }

  // Variables
  var body = document.getElementsByTagName('body')[0],
      elemDomDatum = document.getElementById('desktop-sidekick-1'),
      elemSidebar = document.getElementById('sidebar'),
      elemMain = document.createElement('div'),
      elemHr = document.createElement('hr'),
      elemMainHeadline = document.createElement('h3'),
      elemImgLogoTwitter = document.createElement('img'),
      elemImgLogoArrowLeft = document.createElement('img'),
      elemImgLogoArrowRight = null, // Populated later with .cloneNode();
      elemTweetWrapper = document.createElement('div'),
      elemControlsWrapper = document.createElement('div'),
      staticTweetHtmlCap = buildTweetCap(),
      staticJson = null;

  // Load local (static) JSON
  $.getJSON(globals.constUrlDataStaticJson, function(data){
    if(data){
      staticJson = data;
    }
  });

  // Hit API and proceed with UI rendering
  $.ajax({
    url: globals.constUriTwitterApi,
    dataType: 'json',
    success: function (data){
      if(data && data.results && data.results.length > 0){
        // Create primay UI DOM components
        buildMainWrapper(true);
        // Continue buildling tweet UI components
        renderTweets(data);
      } else {
        if(staticJson !== null){
          /** Render tweets using static json **/
          // Create primay UI DOM components
          buildMainWrapper(true);
          // Continue buildling tweet UI components
          renderTweets(staticJson);
        } else {
          // Create primay UI DOM components
          buildMainWrapper();
          // Continue with fallback UI
          renderFallbackUi();
        }
      }
    },
    error: function(data){
      // Create primay UI DOM components
      buildMainWrapper();
      // Continue with fallback UI
      renderFallbackUi();
    }
  });

  /**
   * Build DOM elements needed if there is tweet data (e.g. API successfully returns data)
   * @param {object} JSON obj containing all the tweet data
   */
  function renderTweets(json){
    // Loop through JSON and create tweets
    generateTweets(json);

    // Add end cap to the twitter feed
    addTweet(staticTweetHtmlCap);

    // Append all children to main wrapper
    compile(true);
  }

  /**
   * Build DOM elements needed if there is a fallback condition (e.g. API returns no data)
   */
  function renderFallbackUi(){
    // Add end cap to the twitter feed
    addTweet(staticTweetHtmlCap);

    // Update DOM elements for fallback scenario
    elemTweetWrapper.classList += 'fallback';
    staticTweetHtmlCap.setAttribute('data-active', '1');

    // Append all children to main wrapper
    compile();
  }

  /**
   * Generate DOM nodes for tweets in JSON object
   * @param {object} JSON obj containing all the tweet data
   */
  function generateTweets(obj){
    // Check to make sure there is data in the json obj
    if(obj && obj.query && obj.query.count && obj.query.count > 0 && obj.query.results.json.statuses){
      var first = true,
          countTweetsRenderd = 0;
      // Handle each tweet in json obj
      $.each(obj.query.results.json.statuses, function(i, v){
        if(this && this.entities.media){
          var tweet = {
            text: this.text,
            userName: this.user.name,
            handle: this.user.screen_name,
            tweetUrl: this.entities.media.url,
            time: moment(this.created_at, 'dd MMM DD HH:mm ZZ YYYY', 'en').format('MMM D @ h:MMa')
          }

          // If this is the first tweet, make sure it's flagged .active in the DOM
          if(first){
            addTweet(buildTweetDefault(tweet, true));
            first = false;
            countTweetsRenderd++; // Maintain count of tweets to determine behavior of tweet cap render
          } else {
            addTweet(buildTweetDefault(tweet, false));
            countTweetsRenderd++; // Maintain count of tweets to determine behavior of tweet cap render
          }
        } else {
          if(countTweetsRenderd < 1){
            // Continue with UI fallback
            renderFallbackUi();
          } else {
            // Continue silently
          }
        }
      });
    } else {
      // Continue with UI fallback
      renderFallbackUi();
    }
  }

  /**
   * Generate markup for main DOM elements
   * @param {bool} flag if controls should be rendered in final app result
   */
  function buildMainWrapper(showControls){
    // Build outer wrapper
    elemMain.setAttribute('id', globals.guid);
    elemMain.classList += 'tweet-injector';

    // Build primary UI elements
    elemMainHeadline.innerHTML = globals.strMainHeadline;
    setAttributes(elemImgLogoTwitter,
      {
        'src': globals.constUrlLogoTwitter,
        'id': 'logo-twitter',
        'title': "Twitter logo"
      });

    if(showControls === true){
      // Build controls
      elemControlsWrapper.setAttribute('id', 'wrapper-controls');
      setAttributes(elemImgLogoArrowLeft,
        {
          'src': globals.constUrlIconArrow,
          'class': 'arrow'
        });

      // Create right arrow based on left arrow elem
      elemImgLogoArrowRight = elemImgLogoArrowLeft.cloneNode();

      // Set attributes on the arrow elems
      setAttributes(elemImgLogoArrowLeft,
        {
          'data-direction': 'left',
          'title': 'Previous tweet'
        });

      setAttributes(elemImgLogoArrowRight,
        {
          'data-direction': 'right',
          'title': 'Next tweet'
        });

      elemImgLogoArrowLeft.addEventListener('click', clickControl);
      elemImgLogoArrowRight.addEventListener('click', clickControl);

      elemControlsWrapper.appendChild(elemImgLogoArrowLeft);
      elemControlsWrapper.appendChild(elemImgLogoArrowRight);
    }

    // Build tweet wrapper + populate with tweets
    elemTweetWrapper.classList += 'wrapper-tweet ';
  }

  /**
   * Builds individual tweet DOM element
   * @param {object} contains tweet data
   * @param {bool} flag identifying that tweet should set as active
   * @return {object} DOM element
   */
  function buildTweetDefault(obj, isActive){
    var wrapperTweet = document.createElement('div'),
        wrapperName = document.createElement('div'),
        wrapperContent = document.createElement('div'),
        wrapperDate = document.createElement('div'),
        h4 = document.createElement('h4'),
        h5 = document.createElement('h5'),
        anchorProfile = document.createElement('a'),
        anchorTweet = document.createElement('a'),
        p = document.createElement('p'),
        small = document.createElement('small');

      // Add classes to inner DOM elems
      wrapperTweet.classList += 'tweet';
      wrapperName.classList += 'wrapper-name';
      wrapperContent.classList += 'wrapper-content';
      wrapperDate.classList += 'wrapper-date';

      // Populate inner DOM elems with tweet values
      h4.innerHTML = obj.userName;
      h5.innerHTML = "@" + obj.handle;
      p.innerHTML = obj.text;
      small.innerHTML = obj.time;

      // Build hyperlink for user profile
      setAttributes(anchorProfile, {
        "href": "https://www.twitter.com/" + obj.handle,
        "title": "Find " +  obj.user + " on Twitter.",
        "target": "_blank"
      });
      anchorProfile.appendChild(h5);

      // Build hyperlink for the tweet
      setAttributes(anchorTweet, {
        "href": obj.tweetUrl,
        "title": "View tweet on Amazon",
        "target": "_blank"
      });
      anchorTweet.appendChild(p);

      // Build wrapper-name
      wrapperName.appendChild(h4);
      wrapperName.appendChild(anchorProfile);

      // Build wrapper-content
      wrapperContent.appendChild(anchorTweet);

      // Build wrapper-date
      wrapperDate.appendChild(small);

      // Assemble final tweet
      wrapperTweet.appendChild(wrapperName);
      wrapperTweet.appendChild(wrapperContent);
      wrapperTweet.appendChild(wrapperDate);

      // If item should be flagged as 'active' add expando attr
      if(isActive){
        wrapperTweet.setAttribute('data-active', '1');
      }

      return wrapperTweet;
  }

  /**
   * Builds DOM elements for the amazon tweet at the end of the collection
   * @return {object} DOM element
   */
  function buildTweetCap(){
    var elemCap = document.createElement('div'),
        elemCapImg = document.createElement('img'),
        elemCapContentWrapper = document.createElement('div'),
        elemCapContentGraf = document.createElement('p');

    elemCap.classList += 'tweet tweet-cap';
    elemCapContentWrapper.classList += 'wrapper-content';
    elemCapContentGraf.innerHTML = globals.strTweetCap;
    elemCapImg.setAttribute('src', globals.constUrlIconAmazon);

    elemCapContentWrapper.appendChild(elemCapContentGraf);
    elemCap.appendChild(elemCapImg);
    elemCap.appendChild(elemCapContentWrapper);

    return elemCap;
  }

  /**
   * Brings together all DOM elements by appending all DOM children nodes to the app wrapper DOM elements
   * @param {bool} flag if controls should be rendered in final app result
   */
  function compile(showControls){
    // Add static html to node
    elemMain.appendChild(elemMainHeadline);
    elemMain.appendChild(elemImgLogoTwitter);
    elemMain.appendChild(elemTweetWrapper);

    // Only show the controls if explicitly stated
    if(showControls === true){
      elemMain.appendChild(elemControlsWrapper);
    }

    // Insert new DOM elems below advert using the first sidebar content container as a datum
    elemSidebar.insertBefore(elemMain, elemDomDatum);
    elemSidebar.insertBefore(elemHr, elemDomDatum);

    // Disable left control arrow
    disablePrev();
  }

  /**
   * Adds tweet DOM elem to the tweet-wrapper DOM elem
   * @param {object} DOM element
   */
  function addTweet(elem){
    elemTweetWrapper.appendChild(elem);
  }

  /**
   * Attaches multiple attributes to a DOM elem
   * @param {object} DOM element
   * @param {array} array of objects with attributes to be attached to DOM element
   */
  function setAttributes(elem, array) {
    for(var key in array) {
      elem.setAttribute(key, array[key]);
    }
  }

  /**
   * Cycles carousel to either prev or next tweet
   * @param {string} direction of carousel movement
   */
  function cycleCarousel(direction){
    var tweets = getElemsTweets(),
        tweetsLen = tweets.length,
        elemActiveTweet = null;

    // Clear any disabled controls
    clearDisabledControls();

    // Find the active tweet
    for(i = 0; i < tweetsLen; i++){
      if(tweets[i].getAttribute('data-active') === '1'){
        elemActiveTweet = tweets[i];
        break;
      }
    }

    switch(direction)
    {
      case 'right':
        setAttributes(elemActiveTweet, {
          'data-stage-left': '1',
          'data-active': '0'
        });
        var nextTweet = elemActiveTweet.nextSibling;
        setAttributes(nextTweet, {
          'data-stage-left': '0',
          'data-active': '1'
        });
        if(nextTweet.nextSibling === null){
          disableNext();
        }
        break;
      case 'left':
        setAttributes(elemActiveTweet, {
          'data-stage-left': '0',
          'data-active': '0'
        });
        var prevTweet = elemActiveTweet.previousSibling;
        setAttributes(prevTweet, {
          'data-stage-left': '0',
          'data-active': '1'
        });
        if(prevTweet.previousSibling === null){
          disablePrev();
        }
        break;
      default:
        break;
    }
  }

  /**
   * Get all arrow control DOM elems
   * @return {array} DOM collection
   */
  function getElemsArrows(){
    return document.querySelectorAll('#' + globals.guid + ' .arrow');
  }
  /**
   * Get all tweet DOM elems
   * @return {array} DOM collection
   */
  function getElemsTweets(){
    return document.querySelectorAll('#' + globals.guid + ' .tweet');
  }

  /**
   * Enables all carousel controls
   */
  function clearDisabledControls(){
    enableNext();
    enablePrev();
  }

  /**
   * Disables next tweet control
   */
  function disableNext(){
    elemImgLogoArrowRight.setAttribute('disabled', true);
  }
  /**
   * Disables prev tweet control
   */
  function disablePrev(){
    elemImgLogoArrowLeft.setAttribute('disabled', true);
  }

  /**
   * Enables next tweet control
   */
  function enableNext(){
    elemImgLogoArrowRight.setAttribute('disabled', false);
  }
  /**
   * Enables prev tweet control
   */
  function enablePrev(){
    elemImgLogoArrowLeft.setAttribute('disabled', false);
  }

  /**
   * Click event handler - cycles the carousel next/prev based on button clicked
   */
  function clickControl(){
    if(this.getAttribute('disabled') !== "true"){
      cycleCarousel(this.getAttribute('data-direction'));
    }
  }
})();
