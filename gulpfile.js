var gulp = require('gulp'),
    less = require('gulp-less'),
    watch = require('gulp-watch'),
    cleanCss = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    path = require('path'),
    htmlmin = require('gulp-htmlmin');

// Compile LESS
gulp.task('less', function(){
  return gulp.src('styles/style.less')
    .pipe(less({
      generateSourceMap: false,
      paths: [ path.join(__dirname, 'less', 'includes')]
    }))
    .pipe(cleanCss()) // Minify output CSS before end of LESS compile
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest('styles'));
});

// Minify HTML
gulp.task('minify', function() {
  return gulp.src(['markup/tweet.html', 'markup/tweet-cap.html'])
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('markup'))
});

// Watch all the things
gulp.task('watch', function(){
  // Watch LESS files
  gulp.watch('styles/*.less', ['less']);
  gulp.watch('markup/*.html', ['minify']);
});

gulp.task('default', function(){
  gulp.start('watch');
});
